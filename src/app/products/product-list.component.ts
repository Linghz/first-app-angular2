import { Component, OnInit } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service'; // importation du service 


@Component({
    // selector: 'pm-products', :: on l'enleve car il n'est plus nested mais appelé par le menu
    moduleId: module.id, // pour remplacer absolute path
    templateUrl : 'product-list.component.html',
    styleUrls : ['product-list.component.css']
})
export class ProductListComponent implements OnInit {
    pageTitle: string ='Product List';
    imageWidth : number = 50;
    imageMargin: number = 2;
    showImage : boolean = false;
    listFilter : string;
    errorMessage: string;
    products: IProduct[];
    

    //initialisation du service dans le constructeur
    constructor(private _productService: ProductService) //ProductService = datatype
    {
    
    }

    toggleImage(): void
    {
        this.showImage = !this.showImage;
    }
    ngOnInit(): void {
        // console.log('In OnInit');
        //appel du service
        // this.products = this._productService.getProducts();
        this._productService.getProducts()
        .subscribe(products => this.products = products, //recup data http
                    error => this.errorMessage = <any>error);  //casting
    }
    onRatingClicked(message:string): void {
        this.pageTitle = "Product List : "+ message;
    }

}